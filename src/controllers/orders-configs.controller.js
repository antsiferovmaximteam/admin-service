const {OrdersConfigsEntity} = require('../repositories/orders-configs.entity');
const {TargetsEntity} = require('../repositories/target.entity');

const formatOrderConfig = ({orders_config_id, targets_id, query, target}) => ({
    orders_config_id,
    targets_id,
    query,
    target_name: target.name
});

class OrdersConfigsController {
    static async getOrdersConfig(req, res) {
        const {exchanges_id} = req.query;

        return exchanges_id ?
            OrdersConfigsController.getByExchange(req, res) :
            OrdersConfigsController.getAll(req, res);
    }

    static async createOrdersConfig(req, res) {
        const ordersConfig = req.body;

        const temp = await OrdersConfigsEntity.create(ordersConfig);

        const options = {where: {orders_config_id: temp.orders_config_id}, include: [TargetsEntity]};
        
        const newOrdersConfig = await OrdersConfigsEntity.find(options);

        res.send({
            data: formatOrderConfig(newOrdersConfig),
            message: `Order config created`
        })
    }

    static async updateOrderConfig(req, res) {
        const {id} = req.params;
        const orderConfig = req.body;

        const options = {where: {orders_config_id: id}, include: [TargetsEntity]};

        await OrdersConfigsEntity.update(orderConfig, options);
        const updatedOrderConfig = await OrdersConfigsEntity.find(options);

        console.log(updatedOrderConfig);

        res.send({
            data: formatOrderConfig(updatedOrderConfig),
            message: `Order config updated`
        })
    }

    static async getAll(req, res) {
        const ordersConfigs = await OrdersConfigsEntity.findAll({include: [TargetsEntity]});

        res.send({
            data: ordersConfigs.map(formatOrderConfig)
        })
    }


    static async getByExchange(req, res) {
        const {exchanges_id} = req.query;

        const ordersConfigs = await OrdersConfigsEntity.findAll({where: {exchanges_id}, include: [TargetsEntity]});

        res.send({
            data: ordersConfigs.map(formatOrderConfig)
        })
    }
}

exports.OrdersConfigsController = OrdersConfigsController;