const {ExchangesEntity} = require('../repositories/exchange.entity');

class ExchangesController {
    static async getExchanges(req, res) {
        const exchanges = await ExchangesEntity.findAll();

        res.send({
            data: exchanges
        });
    }

    static async updateExchanges(req, res) {
        const exchange = req.body;
        const exchanges_id = req.params.id;

        await ExchangesEntity.update(exchange, {
            where: {
                exchanges_id
            },
        });
        const updatedExchange = await ExchangesEntity.find({where: {exchanges_id}});

        res.send({
            data: updatedExchange,
            message: `Exchange ${exchange.name} updated`
        });
    }

    static async deleteExchange(req, res) {
        const exchanges_id = req.params.id;
        const {name} = await ExchangesEntity.find({where: {exchanges_id}});

        await ExchangesEntity.destroy({
            where: {exchanges_id}
        });

        res.send({
            message: `Delete ${name} exchange`,
            data: {exchanges_id: +exchanges_id}
        })
    }

    static async createExchange(req, res) {
        const exchange = req.body;

        const createdExchange = await ExchangesEntity.create(exchange);

        res.send({
            message: `Exchange ${exchange.name} created`,
            data: createdExchange
        });
    }
}

exports.ExchangesController = ExchangesController;