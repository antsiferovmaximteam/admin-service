const {TargetsEntity} = require('../repositories/target.entity');

class TargetsController {
    static async getTargets(req, res) {
        const targets = await TargetsEntity.findAll();

        res.send({
            data: targets
        });
    }

    static async updateTarget(req, res) {
        const {targets_id, ...target} = req.body;

        await TargetsEntity.update(target, {
            where: {
                targets_id
            }
        });

        res.send({
            data: {targets_id, ...target},
            message: `Target {${targets_id}} updated`
        });
    }

    static async deleteTarget(req, res) {
        const targets_id = req.params.id;
        const {name} = await TargetsEntity.find({where: {targets_id}});

        await TargetsEntity.destroy({
            where: {targets_id}
        });

        res.send({
            message: `Delete {${name}} target`,
            data: {targets_id: +targets_id}
        });
    }

    static async createTarget(req, res) {
        const target = req.body;

        const newTarget = await TargetsEntity.create(target);

        res.send({
            message: `Target {${newTarget.name}} created`,
            data: newTarget
        });
    }
}

exports.TargetsController = TargetsController;