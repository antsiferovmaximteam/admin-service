const {CategoriesEntity} = require('../repositories/category.entity');

class CategoriesController {
    static async createCategory(req, res) {
        const category = req.body;

        const createdCategory = await CategoriesEntity.create(category);

        res.send({
            message: `Category {${createdCategory.name}} created`,
            data: createdCategory
        });
    }

    static async getCategories(req, res) {
        const exchanges_id = req.query.exchanges_id;

        return exchanges_id ?
            CategoriesController.getCategoriesByExchange(req, res) :
            CategoriesController.getAllCategories(req, res)
    }

    static async getCategoriesByExchange(req, res) {
        const {exchanges_id} = req.query;

        const categories = await CategoriesEntity.findAll({where: {exchanges_id}});

        res.send({
            data: categories
        });
    }

    static async getAllCategories(req, res) {
        const categories = await CategoriesEntity.findAll();

        res.send({
            data: categories
        })
    }

    static async deleteCategory(req, res) {
        const id = req.params.id;

        await CategoriesEntity.destroy({
            where: {categories_id: id}
        });

        res.send({
            message: 'Category deleted'
        });
    }

    static async updateCategory(req, res) {
        const category = req.body;
        const categories_id = req.params.id;

        await CategoriesEntity.update(category, {
            where: {
                categories_id
            }
        });
        const updatedCategory = await CategoriesEntity.find({where: {categories_id}});

        res.send({
            data: updatedCategory,
            message: `Category {${updatedCategory.name}} updated`
        })
    }
}

exports.CategoriesController = CategoriesController;