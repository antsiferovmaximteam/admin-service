const router = require('express').Router();

const {ExchangesRouter} = require('./exchanges.router');
const {TargetsRouter} = require('./targets.router');
const {CategoriesRouter} = require('./categories.router');
const {OrdersConfigsRouter} = require('./orders-configs.router');

router
    .use('/exchanges', ExchangesRouter)
    .use('/targets', TargetsRouter)
    .use('/categories', CategoriesRouter)
    .use('/orders-configs', OrdersConfigsRouter);

module.exports = router;
