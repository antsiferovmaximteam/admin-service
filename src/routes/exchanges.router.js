const router = require('express').Router();

const {ExchangesController} = require('../controllers/exchanges.controller');
const {asyncMiddleware} = require('../lib/async-middleware');

router
    .get('/', asyncMiddleware(ExchangesController.getExchanges))
    .post('/', asyncMiddleware(ExchangesController.createExchange))
    .put('/:id', asyncMiddleware(ExchangesController.updateExchanges))
    .delete('/:id', asyncMiddleware(ExchangesController.deleteExchange));

exports.ExchangesRouter = router;
