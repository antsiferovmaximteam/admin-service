const router = require('express').Router();

const {OrdersConfigsController} = require('../controllers/orders-configs.controller');
const {asyncMiddleware} = require('../lib/async-middleware');

router
    .get('/', asyncMiddleware(OrdersConfigsController.getOrdersConfig))
    .put('/:id', asyncMiddleware(OrdersConfigsController.updateOrderConfig))
    .post('/', asyncMiddleware(OrdersConfigsController.createOrdersConfig));

exports.OrdersConfigsRouter = router;