const router = require('express').Router();

const {TargetsController} = require('../controllers/targets.controller');
const {asyncMiddleware} = require('../lib/async-middleware');

router
    .get('/', asyncMiddleware(TargetsController.getTargets))
    .post('/', asyncMiddleware(TargetsController.createTarget))
    .put('/', asyncMiddleware(TargetsController.updateTarget))
    .delete('/:id', asyncMiddleware(TargetsController.deleteTarget));

exports.TargetsRouter = router;