const router = require('express').Router();

const {asyncMiddleware} = require('../lib/async-middleware');
const {CategoriesController} = require('../controllers/categories.controller');

router
    .delete('/:id', asyncMiddleware(CategoriesController.deleteCategory))
    .get('/', asyncMiddleware(CategoriesController.getCategories))
    .post('/', asyncMiddleware(CategoriesController.createCategory))
    .put('/:id', asyncMiddleware(CategoriesController.updateCategory));

exports.CategoriesRouter = router;