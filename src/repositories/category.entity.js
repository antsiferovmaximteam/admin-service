const Sequelize = require('sequelize');
const sequelize = require('../config/database');

const Categories = sequelize.define('exchanges', {
    categories_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    name: {
        type: Sequelize.STRING
    },
    url: {
        type: Sequelize.STRING
    },
    exchanges_id: {
        type: Sequelize.INTEGER
    },
    parent_category: {
        type: Sequelize.INTEGER
    },
    is_subcategories: {
        type: Sequelize.BOOLEAN
    }
}, {
    timestamps: false,
    tableName: 'categories'
});

exports.CategoriesEntity = Categories;