const Sequelize = require('sequelize');
const sequelize = require('../config/database');

const {TargetsEntity} = require('./target.entity');

const OrdersConfigsEntity = sequelize.define('orders-config', {
    orders_config_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    targets_id: {
        type: Sequelize.INTEGER
    },
    exchanges_id: {
        type: Sequelize.INTEGER
    },
    query: {
        type: Sequelize.STRING
    }
}, {
    timestamps: false,
    tableName: 'orders_configs',
});

OrdersConfigsEntity.belongsTo(TargetsEntity, {foreignKey: 'targets_id'});

exports.OrdersConfigsEntity = OrdersConfigsEntity;