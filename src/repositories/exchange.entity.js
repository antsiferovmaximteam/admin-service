const Sequelize = require('sequelize');
const sequelize = require('../config/database');

const Exchanges = sequelize.define('exchanges', {
    exchanges_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    name: {
        type: Sequelize.STRING
    },
    url: {
        type: Sequelize.STRING
    }
}, {
    timestamps: false,
    tableName: 'exchanges'
});

exports.ExchangesEntity = Exchanges;