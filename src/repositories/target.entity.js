const Sequelize = require('sequelize');
const sequelize = require('../config/database');

const Targets = sequelize.define('targets', {
    targets_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    name: {
        type: Sequelize.STRING
    }
}, {
    timestamps: false,
    tableName: 'targets'
});

exports.TargetsEntity = Targets;